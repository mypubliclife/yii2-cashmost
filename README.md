cashmost
========
cashmost

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist whitecat636/yii2-cashmost "*"
```

or add

```
"whitecat636/yii2-cashmost": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \whitecat636\cashmost\AutoloadExample::widget(); ?>```