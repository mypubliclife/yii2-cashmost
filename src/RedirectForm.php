<?php
/**
 * @author WhiteCat636
 */

namespace whitecat636\cashmost;

use yii\bootstrap\Widget;
use yii\web\View;

class RedirectForm extends Widget
{
    public $message = 'Now you will be redirected to the payment system.';

    public $api;
    public $invoiceId;
    public $amount;

    public function init()
    {
        parent::init();
        assert(isset($this->api));
        assert(isset($this->invoiceId));
        assert(isset($this->amount));
    }

    public function run()
    {
        $this->view->registerJs("$('#cashmost-checkout-form').submit();", View::POS_READY);
        return $this->render('redirect', [
            'message' => $this->message,
            'api' => $this->api,
            'invoiceId' => $this->invoiceId,
            'amount' => number_format($this->amount, 2, '.', ''),
        ]);
    }
}