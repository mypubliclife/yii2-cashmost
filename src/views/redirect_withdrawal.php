<?php
/**
 * @author WhiteCat636
 *
 * @var \yii\web\View $this
 * @var \whitecat636\cashmost\Api $api
 * @var $invoiceId
 * @var $amount
 * @var $redirectMessage string
 */
?>
<div class="cashmost-checkout">
    <p><?= $message ?></p>
    <form id="cashmost-checkout-form" action="https://cashmost.com/payments/merchantwithdrawal/" method="GET">
        <input type="hidden" name="ShopId" value="<?= $api->shopId ?>">
        <input type="hidden" name="UserId" value="<?= $user_id ?>">
        <input type="hidden" name="UserName" value="<?= $username ?>">
        <input type="hidden" name="Balance" value="<?= $amount ?>">
        <input type="hidden" name="Nonce" value="<?= $nonce ?>">
        <input type="hidden" name="Sign" value="<?= $sign ?>">
<!--        <input type="hidden" name="SuccessUrl" value="--><?//= $api->successUrl ?><!--">-->
<!--        <input type="hidden" name="ErrorUrl" value="--><?//= $api->errorUrl ?><!--">-->
<!--        <input type="hidden" name="StatusUrl" value="--><?//= $api->statusUrl ?><!--">-->
    </form>
</div>
