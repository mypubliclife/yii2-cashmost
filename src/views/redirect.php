<?php
/**
 * @author WhiteCat636
 *
 * @var \yii\web\View $this
 * @var \whitecat636\cashmost\Api $api
 * @var $invoiceId
 * @var $amount
 * @var $redirectMessage string
 */
?>
<div class="cashmost-checkout">
    <p><?= $message ?></p>
    <form id="cashmost-checkout-form" action="https://cashmost.com/payments/create/" method="POST">
        <input type="hidden" name="ShopId" value="<?= $api->shopId ?>">
        <input type="hidden" name="Amount" value="<?= $amount ?>">
        <input type="hidden" name="ShopOrderId" value="<?= $invoiceId ?>">
<!--        <input type="hidden" name="SuccessUrl" value="--><?//= $api->successUrl ?><!--">-->
<!--        <input type="hidden" name="ErrorUrl" value="--><?//= $api->errorUrl ?><!--">-->
<!--        <input type="hidden" name="StatusUrl" value="--><?//= $api->statusUrl ?><!--">-->
    </form>
</div>
