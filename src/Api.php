<?php
/**
 * @author WhiteCat636
 */

namespace whitecat636\cashmost;

use whitecat636\cashmost\events\GatewayEvent;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\Response;

class Api extends \yii\base\Component
{
    /** @var string Id магазина (ex: c4cfc3ed-d402-48fa-a99f-704fe0cc840f) */
    public $shopId;
//    /** @var string Номер счета на сайте продавца. Должен быть уникальным */
//    public $shopOrderId;
    /** @var string Secret string from the PM settings page */
    public $secretKey;

    protected $hash;

    /** @var string Сюда перенаправляется пользователь в случае успешной оплаты */
    public $successUrl;
    /** @var string Сюда перенаправляется пользователь в случае неуспешной оплаты */
    public $errorUrl;
    /** @var string URL для коллбека со статусом платежа */
    public $statusUrl;

    public function init()
    {
        assert(isset($this->shopId));
//        assert(isset($this->shopOrderId));
        assert(isset($this->secretKey));

        $this->errorUrl = Url::to($this->errorUrl, true);
        $this->successUrl = Url::to($this->successUrl, true);
        $this->statusUrl = Url::to($this->statusUrl, true);

        $this->hash = $this->secretKey;
    }


    /**
     * @param array $data
     * @return bool
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function processResult($data)
    {
        if (!$this->checkHash($data))
            throw new ForbiddenHttpException('Hash error');

        $event = new GatewayEvent(['gatewayData' => $data]);

        $this->trigger(GatewayEvent::EVENT_PAYMENT_REQUEST, $event);
        if (!$event->handled)
            throw new HttpException(503, 'Error processing request');

        $transaction = \Yii::$app->getDb()->beginTransaction();
        try {
            $this->trigger(GatewayEvent::EVENT_PAYMENT_SUCCESS, $event);
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
            \Yii::error('Payment processing error: ' . $e->getMessage(), 'CashMost');
            throw new HttpException(503, 'Error processing request');
        }

        return true;
    }
    /**
     * Return result of checking SCI hash
     *
     * @param array $data Request array to check, usually $_POST
     * @return bool
     */
    public function checkHash($data)
    {
        if (!isset(
            $data['ShopId'],
            $data['Amount'],
            $data['ShopOrderId'],
            $data['TransactionId'],
            $data['Status'],
            $data['Sign']
        ))
            return false;

        $params = [
            $data['ShopId'],
            $data['Amount'],
            $data['ShopOrderId'],
            $data['TransactionId'],
            $data['Status'],
        ];

        $sign=hash_hmac('sha512',implode(':', $params),$this->hash);

        if ($sign == $data['Sign'])
            return true;

        \Yii::error('Hash check failed: ' . VarDumper::dumpAsString($params), 'CashMost');
        return false;
    }
    /**
     * @param array $data
     * @return string
     * @throws HttpException
     * @throws \yii\db\Exception
     */

    public function processCreateWithdrawal($data)
    {
        if (!$this->checkWithdrawalHash($data))
            throw new ForbiddenHttpException('Hash error');

        $event = new GatewayEvent(['gatewayData' => $data]);

        $this->trigger(GatewayEvent::EVENT_WITHDRAWAL_REQUEST, $event);
        if (!$event->handled)
            throw new HttpException(503, 'Error processing request');


        return $event->message;
    }
    /**
     * Return result of checking SCI hash
     *
     * @param array $data Request array to check, usually $_POST
     * @return bool
     */
    public function checkWithdrawalHash($data)
    {
        if (!isset(
            $data['TransactionId'],
            $data['UserId'],
            $data['PointId'],
            $data['CurrencyId'],
            $data['Amount'],
            $data['Purse'],
            $data['Nonce'],
            $data['Sign']
        ))
            return false;

        $params = [
            $data['TransactionId'],
            $data['UserId'],
            $data['PointId'],
            $data['CurrencyId'],
            $data['Amount'],
            $data['Purse'],
            $data['Nonce'],
        ];

        $sign=hash_hmac('sha512',implode(':', $params),$this->hash);

        if ($sign == $data['Sign'])
            return true;

        \Yii::error('Hash check failed: ' . VarDumper::dumpAsString($params), 'CashMost');
        return false;
    }


    public function processResultWithdrawal($data)
    {
        if (!$this->checkResultWithdrawalHash($data))
            throw new ForbiddenHttpException('Hash error');

        $event = new GatewayEvent(['gatewayData' => $data]);

        $this->trigger(GatewayEvent::EVENT_WITHDRAWAL_SUCCESS, $event);
        if (!$event->handled)
            throw new HttpException(503, 'Error processing request');


        return $event->message;
    }
    /**
     * Return result of checking SCI hash
     *
     * @param array $data Request array to check, usually $_POST
     * @return bool
     */
    public function checkResultWithdrawalHash($data)
    {
        if (!isset(
            $data['ShopId'],
            $data['ShopOrderId'],
            $data['TransactionId'],
            $data['Status'],
            $data['Sign']
        ))
            return false;

        $params = [
            $data['ShopId'],
            $data['ShopOrderId'],
            $data['TransactionId'],
            $data['Status'],
        ];

        $sign=hash_hmac('sha512',implode(':', $params),$this->hash);

        if ($sign == $data['Sign'])
            return true;

        \Yii::error('Hash check failed: ' . VarDumper::dumpAsString($params), 'CashMost');
        return false;
    }




    /**
     * Performs api call
     *
     * @param string $command Api script name
     * @param array $params Request parameters
     * @return array|bool
     */
    public function call($command, $reqParams = [])
    {

        $secret = $this->secretKey;
        $reqParams['ShopId'] = $this->shopId;
        $reqParams['Nonce'] = time();

        $postJson = json_encode($reqParams);
        $sign = hash_hmac('sha512', $postJson, $secret);
        $url = "https://cashmost.com/api/userapi/$command?sign=$sign";
        $options = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => $postJson
            )
        );
        $context = stream_context_create($options);
        $resJson = file_get_contents($url, false, $context);
        $result = json_decode($resJson, true);
        return $result;
    }

    /**
     * Get account wallet balance
     *
     * @return array|bool
     */
    public function balance()
    {
        return $this->call('getbalance');
    }


//    function apiRequest($command, $reqParams)
//    {
//        $secret = 'xxx';
//        $reqParams['ShopId'] = 'c4cfc3ed-d402-48fa-a99f-704fe0cc840f';
//        $reqParams['Nonce'] = time();
//
//        $postJson = json_encode($reqParams);
//        $sign = hash_hmac('sha512', $postJson, $secret);
//        $url = "https://cashmost.com/api/userapi/$command?sign=$sign";
//        $options = array(
//            'http' => array(
//                'header' => "Content-type: application/json\r\n",
//                'method' => 'POST',
//                'content' => $postJson
//            )
//        );
//        $context = stream_context_create($options);
//        $resJson = file_get_contents($url, false, $context);
//        $result = json_decode($resJson, true);
//        return $result;
//    }

//$response = apiRequest('getbalance', []);
}