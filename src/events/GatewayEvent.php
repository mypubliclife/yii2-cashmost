<?php
/**
 * @author WhiteCat636
 */

namespace whitecat636\cashmost\events;

use yii\base\Event;
use yii\db\ActiveRecord;

class GatewayEvent extends Event
{
    const EVENT_PAYMENT_REQUEST = 'eventPaymentRequest';
    const EVENT_PAYMENT_SUCCESS = 'eventPaymentSuccess';
    const EVENT_WITHDRAWAL_REQUEST = 'eventWithdrawalRequest';
    const EVENT_WITHDRAWAL_SUCCESS = 'eventWithdrawalSuccess';

    /** @var ActiveRecord|null */
    public $invoice;
    /** @var array */
    public $message;
    /** @var array */
    public $gatewayData = [];
}