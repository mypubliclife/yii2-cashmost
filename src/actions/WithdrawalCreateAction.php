<?php
/**
 * @author WhiteCat636
 */

namespace whitecat636\cashmost\actions;

use yii\base\Action;
use yii\base\InvalidConfigException;
use whitecat636\cashmost\Api;
use yii\web\Response;

class WithdrawalCreateAction extends Action
{
    public $componentName;
    public $redirectUrl;

    public $silent = false;

    /** @var Api */
    private $api;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->api = \Yii::$app->get($this->componentName);
        if (!$this->api instanceof Api)
            throw new InvalidConfigException('Invalid CashMost component configuration');

        parent::init();
    }

    public function run()
    {
        try {
            $result = $this->api->processCreateWithdrawal(\Yii::$app->request->post());

            \Yii::$app->response->format = Response::FORMAT_JSON;
            return \Yii::$app->response->send($result);
        } catch (\Exception $e) {
            if (!$this->silent)
                throw $e;
        }

        if (isset($this->redirectUrl))
            return \Yii::$app->response->redirect($this->redirectUrl);
    }
}