<?php
/**
 * @author WhiteCat636
 */

namespace whitecat636\cashmost;

use yii\bootstrap\Widget;
use yii\web\View;

class WithdrawalRedirectForm extends Widget
{
    public $message = 'Now you will be redirected to the payment system.';

    public $api;
    public $user_id;
    public $username;
    public $amount;
    public $nonce;
    public $sign;

    public function init()
    {
        parent::init();
        assert(isset($this->api));
        assert(isset($this->user_id));
        assert(isset($this->username));
        assert(isset($this->amount));
        $this->nonce = 123456789;
        $this->amount = number_format($this->amount, 2, '.', '');
        $this->sign = hash_hmac('sha512', $this->api->shopId.':'.$this->user_id.':'.$this->username.':'.$this->amount.':'.$this->nonce, $this->api->secretKey);
    }

    public function run()
    {
        $this->view->registerJs("$('#cashmost-checkout-form').submit();", View::POS_READY);
        return $this->render('redirect_withdrawal', [
            'message' => $this->message,
            'api' => $this->api,
            'user_id' => $this->user_id,
            'username' => $this->username,
            'nonce' => $this->nonce,
            'sign' => $this->sign,
            'amount' => $this->amount
        ]);
    }
}